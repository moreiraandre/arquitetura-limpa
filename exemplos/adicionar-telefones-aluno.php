<?php

use Moreiraandre\Cleanarch\Infra\Aluno\RepositorioPdo;
use Moreiraandre\Cleanarch\Aplicacao\Aluno\AdicionarTelefoneAlunoDto;
use Moreiraandre\Cleanarch\Aplicacao\Aluno\AdicionarTelefonesAluno;

require 'vendor/autoload.php';

$alunoTelefoneDto = new AdicionarTelefoneAlunoDto(
    cpf: $argv[1],
    ddd: $argv[2],
    numeroTelefone: $argv[3],
);

$repositorioAluno = new RepositorioPdo(new \PDO('sqlite:banco.sqlite'));
$adicionarTelefoneAluno = new AdicionarTelefonesAluno($repositorioAluno);
$adicionarTelefoneAluno->executar($alunoTelefoneDto);