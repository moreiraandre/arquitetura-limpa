<?php

use Moreiraandre\Cleanarch\Aplicacao\Aluno\{MatricularAluno, MatricularAlunoDto};
use Moreiraandre\Cleanarch\Infra\Aluno\RepositorioPdo;

require 'vendor/autoload.php';

$alunoDto = new MatricularAlunoDto(
    cpfAluno: $argv[1],
    nomeAluno: $argv[1],
    emailAluno: $argv[3]
);

$repositorioAluno = new RepositorioPdo(new \PDO('sqlite:banco.sqlite'));
$matricularAluno = new MatricularAluno($repositorioAluno);
$matricularAluno->executar($alunoDto);