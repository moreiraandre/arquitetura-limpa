# Arquitetura Limpa com PHP
Projeto referente ao curso: [PHP e Clean Architecture](https://cursos.alura.com.br/course/php-introducao-clean-achitecture)

Instrutor: [Vinicius Dias](https://www.linkedin.com/in/cviniciussdias/)

[Certificado](https://cursos.alura.com.br/certificate/andre-moreira7/php-introducao-clean-achitecture)

# Docker

## Criar a imagem

```bash
docker-compose build --no-cache # Apenas uma vez
```

## Entrar no terminal do container

```bash
docker-compose run --rm workspace bash
```

# Instalar dependências composer

```bash
composer install
```

# Exemplos

```bash
php exemplos/gerar-arquivos.php
```

> Serão criados arquivos no `./exemplos/tmp`.

# Testes Automatizados

```bash
vendor/bin/phpunit --debug --coverage-text
```