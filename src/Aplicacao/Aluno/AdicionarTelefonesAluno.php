<?php

namespace Moreiraandre\Cleanarch\Aplicacao\Aluno;

use Moreiraandre\Cleanarch\Dominio\Aluno\RepositorioInterface;
use Moreiraandre\Cleanarch\Dominio\CPF;

class AdicionarTelefonesAluno
{
    public function __construct(private readonly RepositorioInterface $repositorioAluno)
    {
    }

    public function executar(AdicionarTelefoneAlunoDto $telefoneDto): void
    {
        $aluno = $this->repositorioAluno->buscarPorCpf(new CPF($telefoneDto->cpf));
        $aluno->adicionarTelefone($telefoneDto->ddd, $telefoneDto->numeroTelefone);

        $this->repositorioAluno->atualizar($aluno);
    }
}