<?php

namespace Moreiraandre\Cleanarch\Aplicacao\Aluno;

class AdicionarTelefoneAlunoDto
{
    public function __construct(
        public readonly string $cpf,
        public readonly string $ddd,
        public readonly string $numeroTelefone
    )
    {
    }
}