<?php

namespace Moreiraandre\Cleanarch\Aplicacao\Aluno;

use Moreiraandre\Cleanarch\Dominio\Aluno\Aluno;
use Moreiraandre\Cleanarch\Dominio\Aluno\RepositorioInterface;

class MatricularAluno
{
    public function __construct(private readonly RepositorioInterface $repositorioAluno)
    {
    }

    public function executar(MatricularAlunoDto $alunoDto): void
    {
        $aluno = Aluno::comCpfNomeEEmail($alunoDto->cpfAluno, $alunoDto->nomeAluno, $alunoDto->emailAluno);
        $this->repositorioAluno->adicionar($aluno);
    }
}