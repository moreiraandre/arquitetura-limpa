<?php

namespace Moreiraandre\Cleanarch\Aplicacao\Aluno;

class MatricularAlunoDto
{
    public function __construct(
        public readonly string $cpfAluno,
        public readonly string $nomeAluno,
        public readonly string $emailAluno
    )
    {
    }
}