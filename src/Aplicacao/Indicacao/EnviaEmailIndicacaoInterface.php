<?php

namespace Moreiraandre\Cleanarch\Aplicacao;

use Moreiraandre\Cleanarch\Dominio\Aluno\Aluno;

interface EnviaEmailIndicacaoInterface
{
    public function enviaParaAlunoIndicado(Aluno $alunoIndicado): void;
}