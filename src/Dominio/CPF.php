<?php

namespace Moreiraandre\Cleanarch\Dominio;

class CPF implements \Stringable
{
    public function __construct(private readonly string $numero)
    {
        $this->setNumero();
    }

    private function setNumero(): void
    {
        $opcoes = [
            'options' => [
                'regexp' => '/\d{3}\.\d{3}\.\d{3}\-\d{2}/'
            ]
        ];
        if (filter_var($this->numero, FILTER_VALIDATE_REGEXP, $opcoes) === false) {
            throw new \InvalidArgumentException('CPF inválido.');
        }
    }

    public function __toString(): string
    {
        return $this->numero;
    }
}