<?php

namespace Moreiraandre\Cleanarch\Dominio\Aluno;

class Telefone implements \Stringable
{
    public function __construct(
        private readonly string $ddd,
        private readonly string $numero,
        private readonly bool $estaSalvoRepositorio = false
    )
    {
        $this->setDdd();
        $this->setNumero();
    }

    public function setDdd(): void
    {
        if (preg_match('/\d{2}/', $this->ddd) !== 1) {
            throw new \InvalidArgumentException('DDD inválido.');
        }
    }

    public function setNumero(): void
    {
        if (preg_match('/\d{8,9}/', $this->numero) !== 1) {
            throw new \InvalidArgumentException('Número de telefone inválido.');
        }
    }

    public function __toString(): string
    {
        return "($this->ddd) $this->numero";
    }

    public function ddd(): string
    {
        return $this->ddd;
    }

    public function numero(): string
    {
        return $this->numero;
    }

    public function estaSalvoRepositorio(): bool
    {
        return $this->estaSalvoRepositorio;
    }
}