<?php

namespace Moreiraandre\Cleanarch\Dominio\Aluno;

use Moreiraandre\Cleanarch\Dominio\CPF;

interface RepositorioInterface
{
    public function adicionar(Aluno $aluno): void;
    public function buscarPorCpf(CPF $cpf): Aluno;
    /** @return Aluno[] */
    public function buscarTodos(): array;
    public function atualizar(Aluno $aluno): void;
}