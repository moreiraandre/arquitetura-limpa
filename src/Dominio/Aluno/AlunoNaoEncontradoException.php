<?php

namespace Moreiraandre\Cleanarch\Dominio\Aluno;

use Moreiraandre\Cleanarch\Dominio\CPF;

class AlunoNaoEncontradoException extends \DomainException
{
    public function __construct(CPF $cpf)
    {
        parent::__construct("Aluno com CPF $cpf não encontrado.");
    }
}