<?php

namespace Moreiraandre\Cleanarch\Dominio\Aluno;

interface CifradorSenhaInterface
{
    public function cifrar(string $senha): string;
    public function comparar(string $senhaNaoCifrada, string $senhaCifrada): bool;
}