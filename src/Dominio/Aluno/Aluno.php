<?php

namespace Moreiraandre\Cleanarch\Dominio\Aluno;

use Moreiraandre\Cleanarch\Dominio\CPF;
use Moreiraandre\Cleanarch\Dominio\Email;

class Aluno
{
    private int $id;
    private string $senha;
    private array $telefones = [];

    public function __construct(
        private readonly CPF    $cpf,
        private readonly string $nome,
        private readonly Email  $email
    )
    {
    }

    public static function comCpfNomeEEmail(string $cpf, string $nome, string $email): self
    {
        return new Aluno(new CPF($cpf), $nome, new Email($email));
    }

    public function adicionarTelefone(string $ddd, string $numero, bool $estaSalvoRepositorio = false): self
    {
        $this->telefones[] = new Telefone($ddd, $numero, $estaSalvoRepositorio);
        return $this;
    }

    public function cpf(): string
    {
        return $this->cpf;
    }

    public function nome(): string
    {
        return $this->nome;
    }

    public function email(): string
    {
        return $this->email;
    }

    /**
     * @return Telefone[]
     */
    public function telefones(): array
    {
        return $this->telefones;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
