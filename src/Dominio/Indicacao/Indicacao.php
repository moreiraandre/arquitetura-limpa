<?php

namespace Moreiraandre\Cleanarch\Dominio\Indicacao;

use Moreiraandre\Cleanarch\Dominio\Aluno\Aluno;

class Indicacao
{
    public function __construct(
        private readonly Aluno $indicante,
        private readonly Aluno $indicado,
        private readonly \DateTimeImmutable $data = new \DateTimeImmutable()
    )
    {
    }
}