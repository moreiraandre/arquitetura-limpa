<?php

namespace Moreiraandre\Cleanarch\Dominio;

class Email implements \Stringable
{
    public function __construct(private readonly string $endereco)
    {
        if (filter_var($this->endereco, FILTER_VALIDATE_EMAIL) === false) {
            throw new \InvalidArgumentException('Endereço de e-mail inválido.');
        }
    }

    public function __toString(): string
    {
        return $this->endereco;
    }
}