<?php

namespace Moreiraandre\Cleanarch\Infra\Aluno;

use Moreiraandre\Cleanarch\Dominio\Aluno\CifradorSenhaInterface;

class CifradorSenhaPhp implements CifradorSenhaInterface
{

    public function cifrar(string $senha): string
    {
        return password_hash($senha, PASSWORD_ARGON2ID);
    }

    public function comparar(string $senhaNaoCifrada, string $senhaCifrada): bool
    {
        return password_verify($senhaNaoCifrada, $senhaCifrada);
    }
}