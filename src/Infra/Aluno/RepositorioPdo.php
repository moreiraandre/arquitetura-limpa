<?php

namespace Moreiraandre\Cleanarch\Infra\Aluno;

use Moreiraandre\Cleanarch\Dominio\Aluno\Aluno;
use Moreiraandre\Cleanarch\Dominio\Aluno\AlunoNaoEncontradoException;
use Moreiraandre\Cleanarch\Dominio\Aluno\RepositorioInterface;
use Moreiraandre\Cleanarch\Dominio\CPF;

class RepositorioPdo implements RepositorioInterface
{
    public function __construct(private readonly \PDO $conexao)
    {
    }

    public function adicionar(Aluno $aluno): void
    {
        $this->adicionarAluno($aluno);
        $this->adicionarTelefones($aluno);
    }

    private function adicionarAluno(Aluno $aluno): void
    {
        $sql = 'INSERT INTO aluno (cpf, nome, email) VALUES (:cpf, :nome, :email);';
        $stmt = $this->conexao->prepare($sql);
        $stmt->bindValue('cpf', $aluno->cpf());
        $stmt->bindValue('nome', $aluno->nome());
        $stmt->bindValue('email', $aluno->email());
        $stmt->execute();
        $alunoId = $this->conexao->lastInsertId();
        $aluno->setId($alunoId);
    }

    public function adicionarTelefones(Aluno $aluno): void
    {
        $sql = 'INSERT INTO telefone (aluno_id, ddd, numero) VALUES (:aluno_id, :ddd, :numero);';
        $stmt = $this->conexao->prepare($sql);
        $stmt->bindValue('aluno_id', $aluno->id());

        foreach ($aluno->telefones() as $telefone) {
            $stmt->bindValue('ddd', $telefone->ddd());
            $stmt->bindValue('numero', $telefone->numero());
            $stmt->execute();
        }
    }

    private function sqlSelectBase(): string
    {
        return <<<SQL
            SELECT 
                aluno.aluno_id, 
                cpf, 
                nome, 
                email, 
                telefone_id, 
                ddd, 
                numero as numero_telefone
            FROM aluno JOIN telefone on aluno.aluno_id = telefone.aluno_id
        SQL;
    }

    public function buscarPorCpf(CPF $cpf): Aluno
    {
        $sql = $this->sqlSelectBase() . ' WHERE aluno.cpf = :cpf';
        $stmt = $this->conexao->prepare($sql);
        $stmt->bindValue('cpf', $cpf);
        $stmt->execute();

        $dadosAluno = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (count($dadosAluno) === 0) {
            throw new AlunoNaoEncontradoException($cpf);
        }

        return $this->mapearAluno($dadosAluno);
    }

    private function mapearAluno(array $dadosAluno): Aluno
    {
        $primeiraLinha = $dadosAluno[0];
        $aluno = Aluno::comCpfNomeEEmail($primeiraLinha['cpf'], $primeiraLinha['nome'], $primeiraLinha['email']);
        $aluno->setId($primeiraLinha['aluno_id']);

        foreach ($dadosAluno as $telefone) {
            $existeTelefone = $telefone['ddd'] !== null && $telefone['numero_telefone'] !== null;
            if ($existeTelefone) {
                $aluno->adicionarTelefone(
                    ddd: $telefone['ddd'],
                    numero: $telefone['numero_telefone'],
                    estaSalvoRepositorio: true
                );
            }
        }

        return $aluno;
    }

    /**
     * @return Aluno[]
     */
    public function buscarTodos(): array
    {
        $stmt = $this->conexao->query($this->sqlSelectBase());

        $listaDadosAluno = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $alunos = [];
        foreach ($listaDadosAluno as $dadosAluno) {
            $cpfAluno = $dadosAluno['cpf'];
            if (!array_key_exists($cpfAluno, $alunos)) {
                $alunos[$cpfAluno] = Aluno::comCpfNomeEEmail(
                    $cpfAluno,
                    $dadosAluno['nome'],
                    $dadosAluno['email']
                );
            }

            $alunos[$cpfAluno]->adicionarTelefone(
                ddd: $dadosAluno['ddd'],
                numero: $dadosAluno['numero_telefone'],
                estaSalvoRepositorio: true
            );
        }

        return array_values($alunos);
    }

    public function atualizar(Aluno $aluno): void
    {
        $sql = <<<SQL
            UPDATE aluno 
            SET 
                cpf   = :cpf, 
                nome  = :nome, 
                email = :email 
            WHERE cpf = :cpf;
        SQL;

        $stmt = $this->conexao->prepare($sql);
        $stmt->bindValue('cpf', $aluno->cpf());
        $stmt->bindValue('nome', $aluno->nome());
        $stmt->bindValue('email', $aluno->email());
        $stmt->execute();

        $sql = 'INSERT INTO telefone (aluno_id, ddd, numero) VALUES (:aluno_id, :ddd, :numero);';
        $stmt = $this->conexao->prepare($sql);
        $stmt->bindValue('aluno_id', $aluno->id());

        foreach ($aluno->telefones() as $telefone) {
            if ($telefone->estaSalvoRepositorio()) {
                continue;
            }

            $stmt->bindValue('ddd', $telefone->ddd());
            $stmt->bindValue('numero', $telefone->numero());
            $stmt->execute();
        }
    }
}