<?php

namespace Moreiraandre\Cleanarch\Infra\Aluno;

use Moreiraandre\Cleanarch\Dominio\Aluno\Aluno;
use Moreiraandre\Cleanarch\Dominio\Aluno\AlunoNaoEncontradoException;
use Moreiraandre\Cleanarch\Dominio\Aluno\RepositorioInterface;
use Moreiraandre\Cleanarch\Dominio\CPF;

class RepositorioEmMemoria implements RepositorioInterface
{
    private array $alunos = [];

    public function adicionar(Aluno $aluno): void
    {
        $this->alunos[] = $aluno;
    }

    public function buscarPorCpf(CPF $cpf): Aluno
    {
        $alunosFiltrados = array_filter($this->alunos, fn(Aluno $aluno) => $aluno->cpf() == $cpf);

        if (count($alunosFiltrados) === 0) {
            throw new AlunoNaoEncontradoException($cpf);
        }

        return $alunosFiltrados[0];
    }

    /**
     * @return Aluno[]
     */
    public function buscarTodos(): array
    {
        return $this->alunos;
    }

    public function atualizar(Aluno $aluno): void
    {
        $alunoEncontrado = $this->buscarPorCpf(new CPF($aluno->cpf()));
        $alunoSubstituido = &$alunoEncontrado;
        $alunoSubstituido = $aluno;
    }
}