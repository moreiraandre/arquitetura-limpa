<?php

namespace Moreiraandre\Cleanarch\Infra\Aluno;

use Moreiraandre\Cleanarch\Dominio\Aluno\CifradorSenhaInterface;

class CifradorSenhaMd5 implements CifradorSenhaInterface
{

    public function cifrar(string $senha): string
    {
        return md5($senha);
    }

    public function comparar(string $senhaNaoCifrada, string $senhaCifrada): bool
    {
        return md5($senhaNaoCifrada) === $senhaCifrada;
    }
}