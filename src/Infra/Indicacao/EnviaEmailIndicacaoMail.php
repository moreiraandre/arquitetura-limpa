<?php

namespace Moreiraandre\Cleanarch\Infra\Indicacao;

use Moreiraandre\Cleanarch\Aplicacao\EnviaEmailIndicacaoInterface;
use Moreiraandre\Cleanarch\Dominio\Aluno\Aluno;

class EnviaEmailIndicacaoMail implements EnviaEmailIndicacaoInterface
{

    public function enviaParaAlunoIndicado(Aluno $alunoIndicado): void
    {
        mail(
            $alunoIndicado->email(),
            '[NossaEscola] - Você foi indicado!',
            <<< MENSAGEM
                <h1>Olá {$alunoIndicado->nome()}, parabéns!!!</h1>
                <br>

                <p>Você foi indicado para estudar na Nossa Escola, e ganhou um desconto exclusivo de 10%.</p>
            MENSAGEM
        );
    }
}