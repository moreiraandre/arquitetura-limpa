CREATE TABLE aluno
(
    aluno_id INTEGER PRIMARY KEY AUTOINCREMENT,
    cpf      TEXT NOT NULL UNIQUE,
    nome     TEXT NOT NULL,
    email    TEXT NOT NULL
);

CREATE TABLE telefone
(
    telefone_id INTEGER PRIMARY KEY AUTOINCREMENT,
    aluno_id    INTEGER NOT NULL,
    ddd         TEXT    NOT NULL,
    numero      TEXT    NOT NULL,
    UNIQUE (ddd, numero),
    FOREIGN KEY (aluno_id) REFERENCES aluno (aluno_id)
);

CREATE TABLE indicacao
(
    indicacao_id       INTEGER PRIMARY KEY AUTOINCREMENT,
    aluno_indicante_id INTEGER NOT NULL,
    aluno_indicado_id  INTEGER NOT NULL,
    data_indicacao     TEXT,
    UNIQUE (aluno_indicante_id, aluno_indicado_id),
    FOREIGN KEY (aluno_indicante_id) REFERENCES aluno (aluno_id),
    FOREIGN KEY (aluno_indicado_id) REFERENCES aluno (aluno_id)
);
