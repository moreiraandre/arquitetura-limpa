<?php

namespace Alura\Arquitetura\Testes\Aplicacao\Aluno;


use Moreiraandre\Cleanarch\Aplicacao\Aluno\MatricularAluno;
use Moreiraandre\Cleanarch\Aplicacao\Aluno\MatricularAlunoDto;
use Moreiraandre\Cleanarch\Dominio\CPF;
use Moreiraandre\Cleanarch\Infra\Aluno\RepositorioEmMemoria;
use PHPUnit\Framework\TestCase;

class MatricularAlunoTest extends TestCase
{
    public function testAlunoDeveSerAdicionadoAoRepositorio()
    {
        $dadosAluno = new MatricularAlunoDto(
            '123.456.789-10',
            'Teste',
            'email@example.com',
        );
        $repositorioDeAluno = new RepositorioEmMemoria();
        $useCase = new MatricularAluno($repositorioDeAluno);

        $useCase->executar($dadosAluno);

        $aluno = $repositorioDeAluno->buscarPorCpf(new CPF('123.456.789-10'));
        $this->assertSame('Teste', (string) $aluno->nome());
        $this->assertSame('email@example.com', (string) $aluno->email());
        $this->assertEmpty($aluno->telefones());
    }
}
